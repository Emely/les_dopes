package body resolutions is

   -----------------------
   -- estChiffreValable --
   -----------------------

   function estChiffreValable
     (g : in Type_Grille;
      v :    Integer;
      c :    Type_Coordonnee)
      return Boolean
   is
      ligne : Integer;
      colonne : Integer;
      carre : Integer;
      cligne : Type_Ensemble;
      ccolonne : Type_Ensemble;
      ccarre : Type_Ensemble;

   begin
      If g(c) /= 0 then
         raise CASE_NON_VIDE;
      end if;
      ligne := obtenirLigne(c);
      colonne := obtenirColonne(c);
      carre := obtenirCarre(c);
      cligne := obtenirChiffresDUneLigne(ligne);
      ccolonne := obtenirChiffresDUneColonne(colonne);
      ccarre := obtenirChiffresDUnCarre(carre);
      return not (appartientChiffre(cligne,v) or appartientChiffre(ccolonne,v) or appartientChiffre(ccarre,v));
   end estChiffreValable;

   -------------------------------
   -- obtenirSolutionsPossibles --
   -------------------------------

   function obtenirSolutionsPossibles
     (g : in Type_Grille;
      c : in Type_Coordonnee)
      return Type_Ensemble
   is
      e : Type_Ensemble;
   begin
      if g(c)/=0 then
         raise CASE_NON_VIDE;
      end if;
      for i in 1..9 loop
         if estChiffreValable(g,c,i)=True then
            e(i):=True;
         else
            e(i):=False;
         end if;
      end loop;
      return e;
   end obtenirSolutionsPossibles;

   ------------------------------------------
   -- rechercherSolutionUniqueDansEnsemble --
   ------------------------------------------

   function rechercherSolutionUniqueDansEnsemble
     (resultats : in Type_Ensemble)
      return Integer
   is
      nb_p : Integer; --nombre de solutions possibles
      r : Integer;
   begin
      nb_p:=0;
      for i in 1..9 loop
         if resultats(i)=True then
            r:=i;
            nb_p:=nb_p+1;
         end if;
      end loop;
      if nb_p=1 then
         return r;
      else
         if nb_p>1 then
            raise PLUS_DE_UN_CHIFFRE;
         else
            raise ENSEMBLE_VIDE;
         end if;
      end if;
   end rechercherSolutionUniqueDansEnsemble;

   --------------------
   -- resoudreSudoku --
   --------------------

   procedure resoudreSudoku
     (g      : in out Type_Grille;
      trouve :    out Boolean)
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "resoudreSudoku unimplemented");
      raise Program_Error with "Unimplemented procedure resoudreSudoku";
   end resoudreSudoku;

end resolutions;
