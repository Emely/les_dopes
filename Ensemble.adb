package body ensemble is

   ------------------------
   -- construireEnsemble --
   ------------------------

   function construireEnsemble return Type_Ensemble is
      e : Type_Ensemble;
   begin
      for i in 1..9 loop
         e(i):=False;
      end loop;
      return e;
   end construireEnsemble;

   ------------------
   -- ensembleVide --
   ------------------

   function ensembleVide (e : in Type_Ensemble) return Boolean is
   begin
      for i in 1..9 loop
         if (e(i) = true) then
            return false;
         end if;
      end loop;
      return true;
   end ensembleVide;

   -----------------------
   -- appartientChiffre --
   -----------------------

   function appartientChiffre
     (e : in Type_Ensemble;
      v :    Integer)
      return Boolean
   is
   begin
      return e(v) ;
   end appartientChiffre;

   --------------------
   -- nombreChiffres --
   --------------------

   function nombreChiffres (e : in Type_Ensemble) return Integer is
   nb : Integer:=0; --nombre d'éléments
   begin
      For i in 1..9 loop
         if e(i)=True then
            nb:=nb+1;
         end if;
      end loop;
      return nb;
   end nombreChiffres;

   --------------------
   -- ajouterChiffre --
   --------------------

   procedure ajouterChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
      if e(v)=True then
         raise APPARTIENT_ENSEMBLE;
      else
         e(v):=True;
      end if;
   end ajouterChiffre;

   --------------------
   -- retirerChiffre --
   --------------------

   procedure retirerChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
      if e(v)=False then
         raise NON_APPARTIENT_ENSEMBLE;
      else
         e(v):=False;
      end if;
   end retirerChiffre;

end ensemble;
