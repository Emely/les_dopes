package body Coordonnee is

   ---------------------------
   -- construireCoordonnees --
   ---------------------------

   function construireCoordonnees
     (ligne   : Integer;
      colonne : Integer)
      return Type_Coordonnee
   is
      c: Type_Coordonnee;
   begin
      c.ligne:=ligne;
      c.colonne:=colonne;
      return c;
   end construireCoordonnees;

   ------------------
   -- obtenirLigne --
   ------------------

   function obtenirLigne (c : Type_Coordonnee) return Integer is
   begin
     return c.ligne;
   end obtenirLigne;

   --------------------
   -- obtenirColonne --
   --------------------

   function obtenirColonne (c : Type_Coordonnee) return Integer is
   begin
      return c.colonne;
   end obtenirColonne;

   ------------------
   -- obtenirCarre --
   ------------------

   function obtenirCarre (c : Type_Coordonnee) return Integer is
   begin
      return (3*(c.ligne-1)/3) + (c.colonne-1)/3 + 1;
   end obtenirCarre;

   ----------------------------
   -- obtenirCoordonneeCarre --
   ----------------------------

   function obtenirCoordonneeCarre
     (numCarre : Integer)
      return Type_Coordonnee
   is
   begin
      if numCarre=1 then
         return (1,1);

      elsif numCarre=2 then
         return (1,4);

      elsif numCarre=3 then
         return (1,7);

      elsif numCarre=4 then
         return (4,1);

      elsif numCarre=5 then
         return (4,4);

      elsif numCarre=6 then
         return (4,7);

      elsif numCarre=7 then
         return (7,1);

      elsif numCarre=8 then
         return (7,4);
      else
         return (7,7);
      end if;

   end obtenirCoordonneeCarre;

end Coordonnee;
