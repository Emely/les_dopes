with ensemble; use ensemble;
with Coordonnee;use Coordonnee;
package body grilleSudoku is

   ----------------------
   -- construireGrille --
   ----------------------

   function construireGrille return Type_Grille is
      g: Type_Grille;
   begin
      for i in 1..9 loop
         for j in 1..9 loop
            g (i,j):=0 ;
         end loop;
      end loop;
      return g;
   end construireGrille;

   --------------
   -- caseVide --
   --------------

   function caseVide
     (g : in Type_Grille;
      c : in Type_Coordonnee)
      return Boolean
   is
   begin
      return g(obtenirLigne(c),obtenirColonne(c))=0;
   end caseVide;

   --------------------
   -- obtenirChiffre --
   --------------------

   function obtenirChiffre
     (g : in Type_Grille;
      c : in Type_Coordonnee)
      return Integer
   is
   begin
      if caseVide(g,c) then
         raise OBTENIR_CHIFFRE_NUL;
      end if;
      return g(obtenirLigne(c),obtenirColonne(c));
   end obtenirChiffre;

   --------------------
   -- nombreChiffres --
   --------------------

   function nombreChiffres (g : in Type_Grille) return Integer is
   z: Integer; -- indice
   begin
      z:=0;
      for i in 1..9 loop
         for j in 1..9 loop
            if g(i,j)/= 0 then
               z:=z+1;
            end if;
         end loop;
      end loop;
      return z;
   end nombreChiffres;

   ------------------
   -- fixerChiffre --
   ------------------

   procedure fixerChiffre
     (g : in out Type_Grille;
      c : in     Type_Coordonnee;
      v : in     Integer)
   is
   begin
      if caseVide(g,c) then
         raise FIXER_CHIFFRE_NON_NUL;
      else
         g(obtenirLigne(c),obtenirColonne(c)) := v;
      end if;
   end fixerChiffre;

   ---------------
   -- viderCase --
   ---------------

   procedure viderCase (g : in out Type_Grille; c : in Type_Coordonnee) is
   begin
      if caseVide(g,c) then
         raise VIDER_CASE_VIDE;
      else
         g(obtenirLigne(c),obtenirColonne(c)):= 0;
      end if;
   end viderCase;

   ----------------
   -- estRemplie --
   ----------------

   function estRemplie (g : in Type_Grille) return Boolean is
   begin
      if nombreChiffres(g) = 81 then
         return True;
      else
         return False;
      end if;
   end estRemplie;

   ------------------------------
   -- obtenirChiffresDUneLigne --
   ------------------------------

   function obtenirChiffresDUneLigne
     (g        : in Type_Grille;
      numLigne : in Integer)
      return Type_Ensemble
   is
      c: Type_Coordonnee;
      ligne: Type_Ensemble; --ligne contenant les chiffres
   begin
      ligne:=construireEnsemble;
      for i in 1..9 loop
         c:=construireCoordonnees(numLigne,i);
         if caseVide(g,c) then
            ajouterChiffre(ligne,obtenirChiffre(g,c));
         end if;
      end loop;
      return ligne;
   end obtenirChiffresDUneLigne;

   --------------------------------
   -- obtenirChiffresDUneColonne --
   --------------------------------

   function obtenirChiffresDUneColonne
     (g          : in Type_Grille;
      numColonne : in Integer)
      return Type_Ensemble
   is
      c: Type_Coordonnee;
      colonne: Type_Ensemble; --colonne contenant les chiffres
      begin
         colonne:=construireEnsemble;
         for i in 1..9 loop
            c:=construireCoordonnees(numColonne,i);
            if caseVide(g,c) then
               ajouterChiffre(colonne,obtenirChiffre(g,c));
            end if;
         end loop;
         return colonne;
   end obtenirChiffresDUneColonne;

   -----------------------------
   -- obtenirChiffresDUnCarre --
   -----------------------------

   function obtenirChiffresDUnCarre
     (g        : in Type_Grille;
      numCarre : in Integer)
      return Type_Ensemble
   is
      c: Type_Coordonnee;
      carre: Type_Ensemble; --carre contenant
      ligne: Integer;
      colonne: Integer;
   begin
      carre:=construireEnsemble;
      c:=obtenirCoordonneeCarre(numCarre);
      colonne:=obtenirColonne(c);
      ligne:=obtenirLigne(c);
      for i in ligne..ligne loop
         for j in colonne..colonne+3 loop
            c := construireCoordonnees (i,j);
            if not caseVide(g,c) then
               ajouterChiffre(carre,(obtenirChiffre(g,c)));
            end if;
         end loop;
      end loop;
      return carre;
   end obtenirChiffresDUnCarre;

end grilleSudoku;
